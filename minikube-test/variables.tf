variable "KUBECONFIG" {
  type    = string
  default = "~/.kube/config"
}
